package com.basim.clearscoretest.utils

import androidx.test.espresso.idling.CountingIdlingResource

/**
 * Static implementation of Espresso counting resource
 */
object CountingIdlingResource {

    private const val RESOURCE = "GLOBAL"

    @JvmField val countingIdlingResource = CountingIdlingResource(RESOURCE)

    //Increment it when a network call has started
    fun increment() {
        countingIdlingResource.increment()
    }

    //Decrement it when network call has finished
    fun decrement() {
        if (!countingIdlingResource.isIdleNow) {
            countingIdlingResource.decrement()
        }
    }
}