package com.basim.clearscoretest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main Application class
 */
@HiltAndroidApp
class MainApplication : Application()