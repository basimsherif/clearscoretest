package com.basim.clearscoretest.di

import android.content.Context
import com.basim.clearscoretest.BuildConfig
import com.basim.clearscoretest.data.local.AppDatabase
import com.basim.clearscoretest.data.local.CreditReportDao
import com.basim.clearscoretest.data.local.LocalDataSource
import com.basim.clearscoretest.data.remote.RemoteDataSource
import com.basim.clearscoretest.data.remote.Service
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
* Module class used for Hilt dependency injection configuration
*/
@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(OkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Singleton
    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    @Provides
    fun provideService(retrofit: Retrofit): Service =
        retrofit.create(Service::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(
        service: Service
    ) = RemoteDataSource(service)

    @Singleton
    @Provides
    fun provideCreditReportDao(db: AppDatabase) = db.creditReportDao()

    @Singleton
    @Provides
    fun provideLocalDataSource(
        creditReportDao: CreditReportDao
    ) = LocalDataSource(creditReportDao)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)
}