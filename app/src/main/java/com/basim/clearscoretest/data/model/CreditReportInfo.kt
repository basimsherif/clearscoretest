package com.basim.clearscoretest.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "credit_report_table")
data class CreditReportInfo(
    @PrimaryKey
    val id: Int = 0,
    val score: Int,
    val status: String,
    val maxScoreValue: Int,
    val minScoreValue: Int
)
