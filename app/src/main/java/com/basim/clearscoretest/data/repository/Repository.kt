package com.basim.clearscoretest.data.repository

import com.basim.clearscoretest.data.local.LocalDataSource
import com.basim.clearscoretest.data.model.CreditReportInfo
import com.basim.clearscoretest.data.remote.RemoteDataSource
import javax.inject.Inject

/**
 * Repository class used to hold all api/localdb operation
 * @param localDataSource local data source
 * @param remoteSource remote data source
 */
class Repository @Inject constructor(
    private val remoteSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) {

    /**
     * Get the score data from API
     */
    suspend fun getCreditDataOnline() =
        remoteSource.getScoreDetails()

    /**
     * Save Data offline
     */
    suspend fun saveDataToDB(creditReportInfo: CreditReportInfo) =
        localDataSource.insertCreditReport(creditReportInfo)

    /**
     * Get the score data from local DB
     */
    suspend fun getCreditDataOffline() =
        localDataSource.getCreditReport()


}