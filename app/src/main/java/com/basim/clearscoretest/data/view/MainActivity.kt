package com.basim.clearscoretest.data.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.basim.clearscoretest.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * We follow single activity concept, and this is the main activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}