package com.basim.clearscoretest.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.basim.clearscoretest.data.model.CreditReportInfo

/**
 * Database class for ROOM
 */
@Database(entities = [CreditReportInfo::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun creditReportDao(): CreditReportDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(appContext: Context): AppDatabase {
            return Room.databaseBuilder(appContext, AppDatabase::class.java, "clearscore_db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}