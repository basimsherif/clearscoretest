package com.basim.clearscoretest.data.remote

import com.basim.clearscoretest.data.model.BaseResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * Service class used for Retrofit
 */
interface Service {
    /***
     * GET method for getting credit score detail from API
     */
    @GET("endpoint.json")
    suspend fun getCreditScoreDetails(): Response<BaseResponse>
}