package com.basim.clearscoretest.data.local

import com.basim.clearscoretest.data.model.CreditReportInfo
import javax.inject.Inject

/**
 * Local source class used to communicate with local DB
 */
class LocalDataSource @Inject constructor(
    private val creditReportDao: CreditReportDao
) {
    /**
     * Insert score data to local DB
     */
    suspend fun insertCreditReport(creditReport: CreditReportInfo) {
        creditReportDao.insert(creditReport)
    }

    /**
     * Get the score data from local DB
     */
    suspend fun getCreditReport(): CreditReportInfo {
        return creditReportDao.getCreditReport()
    }
}