package com.basim.clearscoretest.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.basim.clearscoretest.data.model.CreditReportInfo

/**
 * DAO class for database operations for credit report
 */
@Dao
interface CreditReportDao {

    /**
     * Get most recent credit report data
     */
    @Query("SELECT * FROM credit_report_table LIMIT 1")
    suspend fun getCreditReport(): CreditReportInfo

    /**
     * Insert credit report data to local DB
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(creditReportInfo: CreditReportInfo)
}