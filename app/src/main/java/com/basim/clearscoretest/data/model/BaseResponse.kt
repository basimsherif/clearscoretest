package com.basim.clearscoretest.data.model

import com.squareup.moshi.JsonClass

/**
 * Data class for Base response
 */
@JsonClass(generateAdapter = true)
data class BaseResponse (
    val accountIDVStatus : String,
    val creditReportInfo : CreditReportInfo
)