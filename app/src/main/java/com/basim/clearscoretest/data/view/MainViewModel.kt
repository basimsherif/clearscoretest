package com.basim.clearscoretest.data.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.basim.clearscoretest.data.model.BaseResponse
import com.basim.clearscoretest.data.model.CreditReportInfo
import com.basim.clearscoretest.data.remote.Resource
import com.basim.clearscoretest.data.repository.Repository
import kotlinx.coroutines.launch

/**
 * View model class for Home Fragment
 */
class MainViewModel @ViewModelInject constructor(
    private val repository: Repository
) : ViewModel() {
    private val _creditReportLiveData = MutableLiveData<Resource<CreditReportInfo>>()
    val creditReportLiveData: LiveData<Resource<CreditReportInfo>> = _creditReportLiveData

    /**
     * Get the chart details from API and notify the change to livedata to update UI
     */
    fun getScoreDataOnline() {
        _creditReportLiveData.postValue(Resource.loading())
        viewModelScope.launch {
            val apiResponse = repository.getCreditDataOnline()
            apiResponse.let {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        val data = it.data as BaseResponse
                        _creditReportLiveData.postValue(Resource.success(data.creditReportInfo))
                        repository.saveDataToDB(data.creditReportInfo)
                    }
                    Resource.Status.ERROR -> {
                        _creditReportLiveData.postValue(Resource.error(it.message!!))
                    }
                }
            }
        }
    }

    /**
     * Get the chart details from local DB if available
     */
    fun getScoreDataOffline() {
        _creditReportLiveData.postValue(Resource.loading())
        viewModelScope.launch {
            val data = repository.getCreditDataOffline()
            _creditReportLiveData.postValue(Resource.success(data))
        }
    }
}