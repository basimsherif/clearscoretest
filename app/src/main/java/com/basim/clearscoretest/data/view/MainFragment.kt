package com.basim.clearscoretest.data.view

import android.animation.ObjectAnimator
import android.graphics.LightingColorFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.basim.clearscoretest.R
import com.basim.clearscoretest.data.model.CreditReportInfo
import com.basim.clearscoretest.data.remote.Resource
import com.basim.clearscoretest.data.utils.Utils
import com.basim.clearscoretest.databinding.MainFragmentBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * Fragment class for home screen
 */
@AndroidEntryPoint
class MainFragment : Fragment(R.layout.main_fragment) {
    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: MainFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MainFragmentBinding.bind(view)
        binding.homeViewModel = viewModel
        binding.homeFragment = this
        observeDataAndUpdateUI()
        loadData()
    }

    fun loadData() {
        if (Utils.isNetworkAvailable(requireContext())) {
            //If network available, we will get data online
            viewModel.getScoreDataOnline()
            hideRetry()
        }
        else{
            //If network not available, we will get data offline
            viewModel.getScoreDataOffline()
            showErrorMessage()
        }
    }

    private fun observeDataAndUpdateUI() {
        //Observing livedata for credit score
        viewModel.creditReportLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.loadingView.visibility = View.GONE
                    it.data?.let { data ->
                        binding.scoreProgress.max = data.maxScoreValue
                        binding.maxTextView.text = String.format(
                            getString(R.string.out_of),
                            data.maxScoreValue
                        )
                        animateScoreProgress(data)
                    }
                }
                Resource.Status.ERROR ->
                    showErrorMessage()
                Resource.Status.LOADING ->
                    binding.loadingView.visibility = View.VISIBLE
            }
        })
    }

    private fun showErrorMessage() {
        Toast.makeText(requireContext(), R.string.network_error, Toast.LENGTH_LONG).show()
        binding.retryButton.visibility = View.VISIBLE
    }

    private fun hideRetry(){
        binding.retryButton.visibility = View.GONE
    }

    private fun animateScoreProgress(data: CreditReportInfo) {
        //Configure the animation for score progress bar
        val animation = ObjectAnimator.ofInt(
            binding.scoreProgress,
            "progress",
            0,
            data.score
        )
        //Add a listener to change color of the progress according to the score
        animation.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            var color = if (value <= 200)
                ContextCompat.getColor(requireContext(), R.color.red)
            else if (value <= 400)
                ContextCompat.getColor(requireContext(), R.color.yellow)
            else
                ContextCompat.getColor(requireContext(), R.color.green)
            val drawable: Drawable = binding.scoreProgress.progressDrawable

            drawable.colorFilter = LightingColorFilter(
                -0x1000000,
                color
            )
            binding.scoreText.text = value.toString()
            binding.scoreText.setTextColor(color)
        }
        animation.duration = 5000
        //DecelerateInterpolator for a good looking animation
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }
}