package com.basim.clearscoretest

import com.basim.clearscoretest.data.model.BaseResponse
import com.basim.clearscoretest.data.model.CreditReportInfo
import com.basim.clearscoretest.data.remote.Resource

/**
 * Utility class which contains all utility methods
 */
fun getTestResponse(): Resource<BaseResponse> {
    var creditReportInfo = CreditReportInfo(0,0,"",0,0)
    return Resource.success(BaseResponse("",creditReportInfo))
}

fun getExpectedSuccessTestResponse(): Resource<CreditReportInfo> {
    var creditReportInfo = CreditReportInfo(0,0,"",0,0)
    return Resource.success(creditReportInfo)
}