package com.basim.clearscoretest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.basim.clearscoretest.data.remote.Resource
import com.basim.clearscoretest.data.repository.Repository
import com.basim.clearscoretest.data.view.MainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewmodelTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var repository: Repository

    @Mock
    private lateinit var scoreDataObserver: Observer<Resource<Any>>

    /**
     * Unit test to verify API success scenario while getting chart details from API
     */
    @Test
    fun `given score data should return success`() {
        testCoroutineRule.runBlockingTest {
            doReturn(getTestResponse())
                .`when`(repository)
                .getCreditDataOnline()
            val viewModel = MainViewModel(repository)
            viewModel.creditReportLiveData.observeForever(scoreDataObserver)
            viewModel.getScoreDataOnline()
            verify(scoreDataObserver).onChanged(getExpectedSuccessTestResponse())
            viewModel.creditReportLiveData.removeObserver(scoreDataObserver)
        }
    }

    /**
     * Unit test to verify API error scenario while getting score details from API
     */
    @Test
    fun `given server response should return error`() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Error Message"
            doReturn(Resource.error(errorMessage, null))
                .`when`(repository)
                .getCreditDataOnline()
            val viewModel = MainViewModel(repository)
            viewModel.creditReportLiveData.observeForever(scoreDataObserver)
            viewModel.getScoreDataOnline()
            verify(scoreDataObserver).onChanged(
                Resource.error(errorMessage, null)
            )
            viewModel.creditReportLiveData.removeObserver(scoreDataObserver)
        }
    }

}