package com.basim.clearscoretest

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.basim.clearscoretest.data.local.AppDatabase
import com.basim.clearscoretest.data.local.CreditReportDao
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class InstrumentationTests {
    private lateinit var creditReportDao: CreditReportDao
    private lateinit var db: AppDatabase

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        creditReportDao = db.creditReportDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    /**
     * Instrumented test to verify ROOM for adding score data to local DB and retrieving it
     */
    @Test
    @Throws(Exception::class)
    fun verifyWriteChartAndReadList() = runBlocking {
        //Get the chart list and insert to DB
        val reportdDataFromUtil = Utils.getSampleReportData()
        creditReportDao.insert(reportdDataFromUtil)
        //Wait for LiveData to get notify
        val reportDataFromRoom = creditReportDao.getCreditReport()
        //Assert the result
        assert(reportdDataFromUtil.id == reportDataFromRoom.id)
        assert(reportdDataFromUtil.maxScoreValue == reportDataFromRoom.maxScoreValue)
        assert(reportdDataFromUtil.score == reportDataFromRoom.score)
        assert(reportdDataFromUtil.minScoreValue == reportDataFromRoom.minScoreValue)
        assert(reportdDataFromUtil.status == reportDataFromRoom.status)
    }
}