package com.basim.clearscoretest.pages

import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import com.basim.clearscoretest.R
import com.basim.clearscoretest.data.view.MainActivity
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not


/**
 * This class contains all the functions related to Home page and maintains a Page Object Model
 */
class HomePage {

    var TAG = "Home page"

    /**
     * This function is used to verify home page items
     */
    fun verifyHomePageItems() {
        Log.d(TAG, "Verify if title is shown")
        onView(withId(R.id.titleTextView)).check(matches(withText(R.string.app_name)))
        Log.d(TAG, "Verify if score progress is shown")
        onView(withId(R.id.scoreProgress)).check(matches(isDisplayed()))
        Log.d(TAG, "Verify if description text is shown")
        onView(withId(R.id.yourScoreText)).check(matches(withText(R.string.your_score)))
        Thread.sleep(5000)
        onView(withId(R.id.scoreText)).check(matches(withText("514")))
    }

    /**
     * This function is used to verify error snackbar for offline scenario
     */
    fun verifyErrorToastAndRetryButton(activity: MainActivity) {
        onView(withText(R.string.network_error)).inRoot(
            withDecorView(
                not(
                    `is`(
                        activity.window.decorView
                    )
                )
            )
        ).check(
            matches(
                isDisplayed()
            )
        )
        onView(withId(R.id.retryButton)).check(matches(isDisplayed()))
    }
}