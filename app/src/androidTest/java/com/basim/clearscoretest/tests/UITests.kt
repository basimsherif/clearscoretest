package com.basim.clearscoretest.tests

import androidx.test.espresso.IdlingRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.basim.clearscoretest.data.view.MainActivity
import com.basim.clearscoretest.pages.HomePage
import com.basim.clearscoretest.utils.CountingIdlingResource
import org.junit.*
import org.junit.rules.TestName
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


/**
 * UI Automation test cases to verify major UI flows
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class UITests {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    var currentTestName = TestName()

    //POM object for home page
    private val homePage = HomePage()

    /**
     * Register the IdlingResource for tests to wait for API calls
     */
    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(CountingIdlingResource.countingIdlingResource)
    }

    /**
     * This test case will verify if home page displayed properly
     */
    @Test
    fun verifyHomePageItems() {
        homePage.verifyHomePageItems()
    }

    /**
     * This test case will verify if error message is showing properly in offline scenario
     */
    @Test
    fun verifyIfNoNetworkScenario(){
        //Turn off the WIFI
        homePage.verifyErrorToastAndRetryButton(activityRule.activity)
    }

    /**
     * Unregister the IdlingResource and cleanup the tests
     */
    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(CountingIdlingResource.countingIdlingResource)
        //Turn ON WIFI after offline scenario test
        if(currentTestName.methodName == "verifyIfNoNetworkScenario") {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi enable")
            //Will wait for some time for WIFI reconnection so that other tests wont get affected
            Thread.sleep(3000)
        }
        if(currentTestName.methodName == "verifyHomePageItems") {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi disable")
            Thread.sleep(2000)
        }
    }

}