package com.basim.clearscoretest

import com.basim.clearscoretest.data.model.CreditReportInfo

/**
 * Utility class which contains all utility methods
 */
object Utils {
    /**
     * This will return a mock data for testing Room DB
     */
    fun getSampleReportData():CreditReportInfo{
        return CreditReportInfo(0,512,"",700,0)
    }
}